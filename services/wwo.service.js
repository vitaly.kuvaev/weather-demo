'use strict';

const eachMonthOfInterval = require('date-fns/eachMonthOfInterval');
const endOfMonth = require('date-fns/endOfMonth');
const startOfDay = require('date-fns/startOfDay');
const subDays = require('date-fns/subDays');
const minDate = require('date-fns/min');
const format = require('date-fns/format');
const parse = require('date-fns/parse');

const HTTPClientService = require('moleculer-http-client');
const { MoleculerError } = require('moleculer').Errors;

module.exports = {
	name: 'wwo',
	mixins: [HTTPClientService],
	settings: {
		httpClient: { includeMethods: ['get'] },
		weatherElements: {
			temperature: 'tempC',
			pressure: 'pressure',
			humidity: 'humidity'
		}
	},

	/**
	 * Actions
	 */
	actions: {

		/**
		 * Request World Weather Online historical weather data between the 2 dates inclusive
		 *
		 * @param {Date} startDate - start date
		 * @param {Date} endDate - end date
		 */
		requestWeatherData: {
			params: {
				startDate: 'date',
				endDate: 'date'
			},
			async handler(ctx) {
				// WWO requires start and end date to be in the same month/year.
				// Split the requested period by month and make separate requests
				// for the whole months for caching/storing locally.

				let months = eachMonthOfInterval({
					start: ctx.params.startDate,
					end: ctx.params.endDate
				});

				const capEndDate = endDate => minDate([endDate, subDays(startOfDay(new Date()), 1)]);

				let responses = await Promise.all(months.map(monthStart => {
					let requestParams = {
						key: process.env.WWO_API_KEY,
						q: process.env.LOCATION,
						date: format(monthStart, 'yyyy-MM-dd'),
						enddate: format(capEndDate(endOfMonth(monthStart)), 'yyyy-MM-dd'),
						tp: 24, // 24 hours, day average
						format: 'json'
					};

					let queryString = Object.keys(requestParams)
						.map(k => `${k}=${encodeURIComponent(requestParams[k])}`)
						.join('&');

					return this.actions.get({ url: `${process.env.WWO_API_URL}?${queryString}` });
				}));

				let monthlyWeatherData = [].concat(...responses.map(response => {
					if (response.statusCode !== 200) {
						console.error(response);
						throw new MoleculerError('WWO error', 500, { statusCode: response.statusCode });
					}

					return JSON.parse(response.body).data.weather;
				})).map(dayData => ({
					date: parse(dayData.date, 'yyyy-MM-dd', new Date(0)),
					...Object.assign(...Object.keys(this.settings.weatherElements)
						.map(el => ({ [el]: dayData.hourly[0][this.settings.weatherElements[el]] })))
				}));

				return monthlyWeatherData;
			}
		},
	},
};