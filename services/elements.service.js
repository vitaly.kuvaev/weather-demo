'use strict';

const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
const { MoleculerError } = require('moleculer').Errors;

const startOfDay = require('date-fns/startOfDay');
const endOfDay = require('date-fns/endOfDay');

module.exports = {
	name: 'elements',
	mixins: [DbService],
	adapter: new SqlAdapter(process.env.DATABASE_URL),

	model: {
		name: 'historical',
		define: {
			date: {
				type: Sequelize.DATEONLY,
				primaryKey: true
			},
			temperature: {
				type: Sequelize.INTEGER,
				allowNull: false
			},
			pressure: {
				type: Sequelize.INTEGER,
				allowNull: false
			},
			humidity: {
				type: Sequelize.INTEGER,
				allowNull: false
			}
		},
		options: {
			freezeTableName: true,
			timestamps: false,
			indexes: [
				{
					fields: ['date'],
					unique: true
				}
			]
		}
	},

	/**
	 * Actions
	 */
	actions: {

		/**
		 * Find stored weather data between the dates provided
		 *
		 * @param {Data} startDate - start date
		 * @param {Date} endDate - end date
		 */
		find: {
			params: {
				startDate: 'date',
				endDate: 'date'
			},
			async handler(ctx) {
				let data;

				try {
					data = await this.adapter.find({
						query: {
							date: {
								[Sequelize.Op.between]: [startOfDay(ctx.params.startDate), endOfDay(ctx.params.endDate)]
							}
						},
						sort: ['date']
					});
				} catch (e) {
					console.error(e);
					throw new MoleculerError('Database error', 500);
				}

				return data;
			}
		},

		/**
		 * Store weather data series
		 *
		 * @param {Array} weatherData - weather data
		 */
		store: {
			params: {
				weatherData: 'array'
			},
			async handler(ctx) {
				try {
					await this.adapter.insertMany(ctx.params.weatherData);
				} catch (e) {
					console.error(e);
					throw new MoleculerError('Database error', 500);
				}
			}
		}
	},
};