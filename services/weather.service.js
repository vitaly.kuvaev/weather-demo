'use strict';

const format = require('date-fns/format');
const parse = require('date-fns/parse');
const startOfDay = require('date-fns/startOfDay');
const addDays = require('date-fns/addDays');
const subDays = require('date-fns/subDays');
const isEqual = require('date-fns/isEqual');
const isAfter = require('date-fns/isAfter');
const minDate = require('date-fns/min');

const { MoleculerError } = require('moleculer').Errors;

module.exports = {
	name: 'weather',

	/**
	 * Actions
	 */
	actions: {

		/**
		 * Request historical weather data between the 2 dates inclusive
		 *
		 * @param {String} startDate - start date
		 * @param {String} endDate - end date
		 */
		historical: {
			params: {
				startDate: 'string',
				endDate: 'string'
			},
			async handler(ctx) {
				let startDate = parse(ctx.params.startDate, 'yyyy-MM-dd', new Date(0)),
					endDate = parse(ctx.params.endDate, 'yyyy-MM-dd', new Date(0));

				// cap end date to yesterday as WWO won't have historical data beyond that;
				// check that start date is before end date

				endDate = minDate([endDate, subDays(startOfDay(new Date()), 1)]);

				if (isAfter(startDate, endDate)) {
					throw new MoleculerError('Start date should be before the end date', 400);
				}

				// get stored weather data between the dates, fill in the gaps, if any, from WWO

				let storedData = await this.broker.call('elements.find', { startDate, endDate }),
					storedIdx = 0,
					nextStoredDate,
					nextDate = startDate,
					completeSeries = [],
					gaps = [];

				if (storedData.length == 0) {
					gaps.push({
						startDate,
						endDate,
						startIndex: 0
					});
				} else {
					while (storedIdx < storedData.length) {
						// sequelize returns dates as strings
						nextStoredDate = parse(storedData[storedIdx].date, 'yyyy-MM-dd', new Date(0));

						if (!isEqual(nextDate, nextStoredDate)) {
							gaps.push({
								startDate: nextDate,
								endDate: subDays(nextStoredDate, 1),
								startIndex: completeSeries.length
							});
							nextDate = nextStoredDate;
						}

						completeSeries.push(storedData[storedIdx++]);
						nextDate = addDays(nextDate, 1);
					}

					if (!isEqual(nextDate, addDays(endDate, 1))) {
						gaps.push({
							startDate: nextDate,
							endDate,
							startIndex: completeSeries.length
						});
					}
				}

				let gapFills = await Promise.all(gaps.map(g => (async gap => {
					console.log(`Requesting data from WWO from ${gap.startDate} to ${gap.endDate}`);
					gap.wwoData = await this.broker.call('wwo.requestWeatherData', {
						startDate: gap.startDate,
						endDate: gap.endDate
					});

					return gap;
				})(g)));

				gapFills.forEach(gap => completeSeries.splice(gap.startIndex, 0, ...gap.wwoData));

				// store series data received from WWO
				await Promise.all(gaps.map(g => this.broker.call('elements.store', { weatherData: g.wwoData })));

				completeSeries.forEach(item => {
					if (typeof item.date != 'string') {
						item.date = format(item.date, 'yyyy-MM-dd')
					}
				});

				return completeSeries;
			}
		}
	},
};